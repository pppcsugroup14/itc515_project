
//Changed By Gopal Bogati
package library;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import itc_project.library.borrowbook.BorrowBookUI;
import itc_project.library.borrowbook.BorrowBookControl;
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookUI;
import library.fixbook.FixBookControl;
import library.payfine.PayFineUI;
import library.payfine.PayFineControl;
import library.returnBook.ReturnBookControl;
import library.returnBook.ReturnBookUI;



public class Main {
    private static Scanner IN;
    private static Library LIB;
    private static String MENU; 
    private static Calendar CAL;
    private static SimpleDateFormat SDF;
        
        private static String getMenu() {//get_Menu ->getMenu
	    StringBuilder sb = new StringBuilder();
	        sb.append("\nLibrary Main Menu\n\n")
		  .append("  M  : add member\n")
		  .append("  LM : list members\n")
		  .append("\n")
		  .append("  B  : add book\n")
		  .append("  LB : list books\n")
		  .append("  FB : fix books\n")
		  .append("\n")
		  .append("  L  : take out a loan\n")
		  .append("  R  : return a loan\n")
		  .append("  LL : list loans\n")
		  .append("\n")
		  .append("  P  : pay fine\n")
		  .append("\n")
		  .append("  T  : increment date\n")
		  .append("  Q  : quit\n")
		  .append("\n")
		  .append("Choice : ");
		  
		return sb.toString();
	}


	public static void main(String[] args) {		
	    try {			
		IN = new Scanner(System.in);
		LIB = Library.getInstance();
		CAL = Calendar.getInstance();
		SDF = new SimpleDateFormat("dd/MM/yyyy");
	
	        for (Member m : LIB.listMembers()) {
	                  output(m);
			 }
			   output(" ");
		     for (Book b : LIB.listBooks()) {
			   output(b);
			 }			
			   MENU = getMenu();			
			   boolean e = false;
			   while (!e) {
		               output("\n" + SDF.format(CAL.getDate()));
			       String c = input(MENU);
			       switch (c.toUpperCase()) {	
                                       case "M": 
				          addMember();//ADD_MEMBER->addMember
				          break;					
				       case "LM": 
					  listMembers();//LIST_MEMBERS->listMembers
					  break;					
				       case "B": 
					  addBook();//ADD_BOOK->addBook
					  break;					
				       case "LB": 
					  listBooks(); //LIST_BOOKS->listBooks
					  break;
				       case "FB": 
					  fixBooks();//FIX_BOOKS->fixBooks
					  break;					
				       case "L": 
					  borrowBook();//BORROW_BOOK->borrowBook
					  break;
				       case "R": 
					  returnBook();//RETURN_BOOK->returnBook
					  break;
				       case "LL": 
					  listCurrentLoans();//LIST_CURRENT_LOANS->listCurrentLoans
					  break;
				       case "P": 
					  payFines();//PAY_FINES->payFines
					  break;					
				       case "T": 
					  incrementDate();//INCREMENT_DATE->incrementDate
					  break;
				       case "Q": 
					  e = true;
					  break;
					
				       default: 
					  output("\nInvalid option\n");
					  break;
				}
				
				Library.save();
			}			
		} catch (RuntimeException e) {
			output(e);
		}		
		output("\nEnded\n");
	}	

	
	private static void payFines() {

		new PayFineUI(new PayFineControl()).run();	//pAY_fINE_cONTROL->PayFineControl
	}


	private static void listCurrentLoans() { //List_current_loans ->listCurrentLoans
		output("");
		for (Loan loan : LIB.listCurrentLoans()) {
			output(loan + "\n");
		}		
	}



	private static void listBooks() {//List_books ->listBooks
		output("");
		for (Book book : LIB.listBooks()) {
			output(book + "\n");
		}		
	}



	private static void listMembers() {//List_MEMBERS ->listMembers
		output("");
		for (Member member : LIB.listMembers()) {
			output(member + "\n");
		}		
	}



	private static void borrowBook() {//BORROW_BOOk ->borrowBook
		new BorrowBookUI(new BorrowBookControl()).run();		
	}


	private static void returnBook() { //Return_Book -> returnBook
		new ReturnBookUI(new ReturnBookControl()).run();//rETURN_bOOK_cONTROL->returnBookControl	
	}


	private static void fixBooks() {
		new FixBookUI(new FixBookControl()).run();//fix_Book_COntrol ->FixBookControl	
	}


	private static void incrementDate() { //IncreMent_Date ->incrementDate
		try {
		        int days = Integer.valueOf(input("Enter number of days: ")).intValue();
			CAL.incrementDate(days);
			LIB.checkCurrentLoans();
			output(SDF.format(CAL.getDate()));
			
		 } catch (NumberFormatException e) {
			output("\nInvalid number of days\n");
		  }
	}
	private static void addBook() { //ADD_books->addBook
		String author = input("Enter author: ");
		String title  = input("Enter title: ");
		String callNumber = input("Enter call number: ");
		Book BoOk = LIB.addBook(author, title, callNumber); //AuThOr->author,title->title,callNumber->callNumber
		output("\n" + BoOk + "\n");
		
	}
	private static void addMember() { //add_MEMBER->addMember
	    try {
	        String lastName = input("Enter last name: ");
		String firstName  = input("Enter first name: ");
		String emailAddress = input("Enter email address: ");
		int phoneNumber = Integer.valueOf(input("Enter phone number: ")).intValue();
		Member member = LIB.addMember(lastName, firstName, emailAddress, phoneNumber);//LaSt_NaMe->lastName, firstName->firstName, emailAddress ->emailAddress,phoneNumber->phoneNumber
		output("\n" + member + "\n"); //MeMbEr->member
			
	    } catch (NumberFormatException e) {
			 output("\nInvalid phone number\n");
		}
		
	}


	private static String input(String prompt) {
	    System.out.print(prompt);
	    return IN.nextLine();
	}
		
	private static void output(Object object) {
	    System.out.println(object);
	}

	
}
