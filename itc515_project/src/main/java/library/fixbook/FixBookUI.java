
//Changed By Arjun Silwal
package library.fixbook;
import java.util.Scanner;

public class FixBookUI {

	public static enum uiState { INITIALISED, READY, FIXING, COMPLETED };
	private FixBookControl control;
	private Scanner input;//INput->input
	private uiState state;//StAte->state
        
        public FixBookUI(FixBookControl control) {//CoNtRoL->control
	    this.control = control;
            input = new Scanner(System.in);
	    state = uiState.INITIALISED;
            control.setUi(this);
	}


	public void setState(uiState state) {
	    this.state = state;
	}

	public void run() {
	    output("Fix Book Use Case UI\n");
	     while (true) {
		switch (state) {
		    case READY:
		       String bookEntryString = input("Scan Book (<enter> completes): ");//BoOk_EnTrY_StRiNg->bookEntryString
		       if (bookEntryString.length() == 0) 
		           control.scanningComplete();	
		       else {
		            try {
			        int bookId = Integer.valueOf(bookEntryString).intValue();//BoOk_Id->bookId
						control.bookScanned(bookId);
				}
			     catch (NumberFormatException e) {
						output("Invalid bookId");
				}
			    }
			break;	
		
		    case FIXING:
			String AnS = input("Fix Book? (Y/N) : ");
			boolean FiX = false;
			if (AnS.toUpperCase().equals("Y")) 
			    FiX = true;
			    control.fixBook(FiX);
			break;
								
		    case COMPLETED:
			output("Fixing process complete");
			return;
			
		     default:
			output("Unhandled state");
			throw new RuntimeException("FixBookUI : unhandled state :" + state);			
			
			}		
		}
		
	}

	
	private String input(String prompt) { //iNpUt->input
	    System.out.print(prompt);
	    return input.nextLine();
	}	
		
		
	private void output(Object object) {//OuTpUt->output
	    System.out.println(object);
	}
	

	public void display(Object object) {
	    output(object);
        }
}
