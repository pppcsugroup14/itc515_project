// Edited By Arjun
package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl { //    fix_BOOK_cONTROL->FixBookControl
	
	private FixBookUI Ui;
	private enum controlState { INITIALISED, READY, FIXING };//CoNtRoL_StAtE->controlState//CoNtRoL_StAtE->controlState
	private controlState state;//StAte->state
	private Library library;//liBrary->library
	private Book currentBook;//CuRrEnT_BoOk->currentBook
	public FixBookControl() {//Fix_Book_Control->FixBookControl
	    this.library = Library.getInstance(); //GEt_InstaNCE->getInstance
	    state = controlState.INITIALISED;
	}
	
	
	public void setUi(FixBookUI ui) { //SeT_Ui->setUi
		if (!state.equals(controlState.INITIALISED)) 
		    throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
		    this.Ui = ui;
		    ui.setState(FixBookUI.uiState.READY);//uI_sTaTe->uiState, setState->setState
		    state = controlState.READY;		
	}


	public void bookScanned(int bookId) {//BoOk_ScAnNeD->bookScanned,bookId->bookId
		if (!state.equals(controlState.READY)) 
	            throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
		    currentBook = library.getBook(bookId);
		
		if (currentBook == null) {
		    Ui.display("Invalid bookId");//dIsPlAy->display
		    return;
		}
		if (!currentBook.isDamaged()) {
		    Ui.display("Book has not been damaged");
		    return;
		}
		Ui.display(currentBook.toString());
		Ui.setState(FixBookUI.uiState.FIXING);
		state = controlState.FIXING;		
	}


	public void fixBook(boolean mustFix) {//FiX_BoOk->fixBook
		if (!state.equals(controlState.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mustFix) //mUsT_FiX->mustFix
			library.repairBook(currentBook);		
		      currentBook = null;
		Ui.setState(FixBookUI.uiState.READY);
		state = controlState.READY;		
	}

	
	public void scanningComplete() {//SCannING_COMplete->scanningComplete
		if (!state.equals(controlState.READY)) 
		    throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		Ui.setState(FixBookUI.uiState.COMPLETED);		
	}

}
