//Changed by Gopal Bogati
package library.entities;
import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {
    private String title;//Title->title
    private String author;//AuthOR->author
    private String callNo;//CallNo->callNo
    private int id;//id->Id
    private enum State { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };
    private State state;//State->state
	
    public Book(String author, String title, String callNo, int id) {
        this.author = author;
        this.title = title;
        this.callNo = callNo;
        this.id = id;
        this.state = State.AVAILABLE;
	}
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Book: ").append(id).append("\n")
          .append("  Title:  ").append(title).append("\n")
          .append("  Author: ").append(author).append("\n")
          .append("  CallNo: ").append(callNo).append("\n")
          .append("  State:  ").append(state);
        return sb.toString();
	}

    public Integer getId() {//GET_id ->getId changed
        return id;
	}

    public String getTitle() {//Get_Title ->getTitle changed
        return title;
	}
    
    public boolean isAvailable() {//IsaVailable ->isAvailable changed
        return state == State.AVAILABLE;
	}
    
    public boolean isOnLoan() {//IS_ON_loan ->isOnLoan changed
	return state == State.ON_LOAN;
	}

    public boolean isDamaged() {//Is_DAMAGED ->isDamaged changed
	return state == State.DAMAGED;
	}

    public void borrow() {//bOrrow ->borrow changed
	if (state.equals(State.AVAILABLE)) 
	    state = State.ON_LOAN;
	else 
     	    throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));	
	}


    public void ReTuRn(boolean damaged) {//ReTurn ->returnLoan changed
	if (state.equals(State.ON_LOAN)) 
	    if (damaged) 
	       state = State.DAMAGED;	
	    else 
	       state = State.AVAILABLE;
        else 
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
				
	}

	
    public void repair() {  //REPAIR->repair
        if (state.equals(State.DAMAGED)) 
	    state = State.AVAILABLE;	
	else 
	    throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
		
	}


}