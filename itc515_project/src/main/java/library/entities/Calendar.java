
//Edited By Arjun
package library.entities;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Calendar {
    private static Calendar self;//Changing sElF to self
    private static java.util.Calendar calendar;/*changing calendar variable into lowercase--Arjun*/	
    private Calendar() {
        calendar = java.util.Calendar.getInstance(); //Another same calendar variable changed --Arjun
    }
	
    public static Calendar getInstance() {/*changing method name to getInstance --Arjun*/
        if (self == null) {
            self = new Calendar();
        }
        return self;
    }
	
    public void incrementDate(int days) {
        calendar.add(java.util.Calendar.DATE, days);//All the calendar variable to same lowercase --Arjun		
    }
	
    public synchronized void setDate(Date date) { //changing DaTe to date--Arjun
        /*changing setDate method name to setDate--Arjun*/
        try {
            calendar.setTime(date); 
            calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  
            calendar.set(java.util.Calendar.MINUTE, 0);  
            calendar.set(java.util.Calendar.SECOND, 0);  
            calendar.set(java.util.Calendar.MILLISECOND, 0);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }	
    }
    public synchronized Date getDate() { /*changing method name to getDate--Arjun*/
        try {
        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);  
        calendar.set(java.util.Calendar.MINUTE, 0);  
        calendar.set(java.util.Calendar.SECOND, 0);  
        calendar.set(java.util.Calendar.MILLISECOND, 0);
        return calendar.getTime();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }	
    }

    public synchronized Date getDueDate(int loanPeriod) {/*changing method name getDueDate--Arjun*/
        Date now = getDate();//changing nOw to now--Arjun
        calendar.add(java.util.Calendar.DATE, loanPeriod);
        Date dueDate = calendar.getTime();
        calendar.setTime(now);
        return dueDate;
    }
	
    public synchronized long getDaysDifference(Date targetDate) {
        /*changing getDaysDifference to getDaysDifference--Arjun*/
        long diffMillis = getDate().getTime() - targetDate.getTime();//Diff_Millis changed to diffMillis
        long diffDays = TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);//Diff_Days to diffDays
        return diffDays;
    }

}
