//Changed By Gopal Bogati
package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import library.entities.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
     public static enum loanState { CURRENT, OVER_DUE, DISCHARGED };
	
        private int loanId; //LoAn_Id ->loanId
        private Book book; //Bo0k -> book
        private Member member; //MeMber -> member
        private Date date; // DaTe -> date
        private loanState state; // StaTe -> state
   
      public Loan(int loanId, Book book, Member member, Date dueDate) { //DuE_dAtE ->dueDate
          this.loanId = loanId; //LoAnId->loanId
	  this.book = book;//BOOK->book
	  this.member = member;//MeMber->member
	  this.date = dueDate;//DueDate->dueDate
	  this.state = loanState.CURRENT;
	}

	
	public void isCheckOverDue() {//cHeCk_OvEr_DuE ->isCheckOverDue
	    if (state == loanState.CURRENT &&
	        Calendar.getInstance().getDate().after(date)) 
	        this.state = loanState.OVER_DUE;			
	  }

	
	public boolean isOverDue() { //Is_OvEr_DuE->isOverDue
	     return state == loanState.OVER_DUE;
	}

	
	public Integer getId() { //GeT_Id->getId
	     return loanId;
	}


	public Date getDueDate() { // GeT_DuE_DaTe->getDueDate
	     return date;
	}
	

	public String toString() {
	     SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
             StringBuilder sb = new StringBuilder();
	     sb.append("Loan:  ").append(loanId).append("\n")
                 .append("  Borrower ").append(member.getId()).append(" : ")
	         .append(member.getLastName()).append(", ").append(member.getFirstName()).append("\n")
	         .append("  Book ").append(book.getId()).append(" : " )
	         .append(book.getTitle()).append("\n")
	         .append("  DueDate: ").append(sdf.format(date)).append("\n")
	         .append("  State: ").append(state);		
	     return sb.toString();
	}


	public Member getMember() { //GeT_MeMbEr->getMember
	     return member;
	}


	public Book getBook() { // GeT_BoOk->getBook
	     return book;
	}


	public void discharge() { //DiScHaRgE->discharge
	     state = loanState.DISCHARGED;		
	}

}