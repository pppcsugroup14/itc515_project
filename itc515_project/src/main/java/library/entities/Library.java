//Changed By Kripa Gosai
package library.entities;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import library.entities.Calendar;
import java.io.ObjectOutputStream;
import library.entities.Book;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
	
	private static final String libraryFile = "library.obj"; /*lIbRaRyFiLe-> libraryFile*/
	private static final int loanLimit = 2; /*change lOaNlImIt-> loanLimit*/
	private static final int loanPeriod = 2;
	private static final double finePerDay = 1.0; /*FiNe_PeR_DaY-> finePerDay*/
	private static final double maxFinesOwed = 1.0;
	private static final double damageFee = 2.0;
	
	private static Library SeLf;
	private int bookId; /*bOoK_Id->bookId*/
	private int memberId;/*mEmBeR_Id->memberId*/
	private int loanId; /*lOaN_Id to loanId*/
	private Date loanDate;
	
	private Map<Integer, Book> catalog;
	private Map<Integer, Member> members;
	private Map<Integer, Loan> loans;
	private Map<Integer, Loan> currentLoans;
	private Map<Integer, Book> damagedBooks;
	

	private Library() {
		catalog = new HashMap<>();
		members = new HashMap<>();
		loans = new HashMap<>();
		currentLoans = new HashMap<>();
		damagedBooks = new HashMap<>();
		bookId = 1;
		memberId = 1;		
		loanId = 1;		
	}

	
	public static synchronized Library getInstance() {		
		if (SeLf == null) {
			Path PATH = Paths.get(libraryFile);			
			if (Files.exists(PATH)) {	
				try (ObjectInputStream LiBrArY_FiLe = new ObjectInputStream(new FileInputStream(libraryFile));) {
			    
					SeLf = (Library) LiBrArY_FiLe.readObject();
					Calendar.getInstance().setDate(SeLf.loanDate);
					LiBrArY_FiLe.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else SeLf = new Library();
		}
		return SeLf;
	}

	
	public static synchronized void save() {
		if (SeLf != null) {
			SeLf.loanDate = Calendar.getInstance().getDate();
			try (ObjectOutputStream LiBrArY_fIlE = new ObjectOutputStream(new FileOutputStream(libraryFile));) {
				LiBrArY_fIlE.writeObject(SeLf);
				LiBrArY_fIlE.flush();
				LiBrArY_fIlE.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public int getBookId() {/*gEt_BoOkId->getBookId*/
		return bookId;
	}
	
	
	public int getMemberId() { /*gEt_MeMbEr_Id->getMemberId*/
		return memberId;
	}
	
	
	private int getNextBookId() { /*gEt_NeXt_BoOk_Id->getNextBookId*/
		return bookId++;
	}

	
	private int getNextMemberId() { /*gEt_NeXt_MeMbEr_Id->getNextMemberId*/
		return memberId++;
	}

	
	private int getNextLoanId() { 
		return loanId++;
	}

	
	public List<Member> listMembers() {		
		return new ArrayList<Member>(members.values()); 
	}


	public List<Book> listBooks() {		
		return new ArrayList<Book>(catalog.values()); 
	}


	public List<Loan> listCurrentLoans() {
		return new ArrayList<Loan>(currentLoans.values());
	}


	public Member addMember(String lastName, String firstName, String email, int phoneNo) {		
		Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberId());
		members.put(member.getId(), member);		
		return member;
	}

	
	public Book addBook(String a, String t, String c) {		
		Book b = new Book(a, t, c, getNextBookId());
		catalog.put(b.getId(), b);		
		return b;
	}

	
	public Member getMember(int memberId) {
		if (members.containsKey(memberId)) 
			return members.get(memberId);
		return null;
	}

	
	public Book getBook(int bookId) {
		if (catalog.containsKey(bookId)) 
			return catalog.get(bookId);		
		return null;
	}

	
	public int getLoanLimit() {
		return loanLimit;
	}

	
	public boolean canMemberBorrow(Member member) {		
		if (member.getNumberOfCurrentLoans() == loanLimit ) 
			return false;
				
		if (member.finesOwed() >= maxFinesOwed) 
			return false;
				
		for (Loan loan : member.getLoans()) 
			if (loan.isOverDue()) 
				return false;
			
		return true;
	}

	
	public int getNumberOfLoansRemainingForMember(Member member) {	/*MeMbEr->newMembers*/		
		return loanLimit - member.getNumberOfCurrentLoans();
	}

	
	public Loan issueLoan(Book book, Member member) {
		Date dueDate = Calendar.getInstance().getDueDate(loanPeriod);
		Loan loan = new Loan(getNextLoanId(), book, member, dueDate);
		member.takeOutLoan(loan);
		book.borrow();
		loans.put(loan.getId(), loan);
		currentLoans.put(book.getId(), loan);
		return loan;
	}
	
	
	public Loan getLoanByBookId(int bookId) { /*GeT_LoAn_By_BoOkId->getLoanByBookId*/
		if (currentLoans.containsKey(bookId)) /*cHeCk_CuRrEnT_LoAnS->checkCurrentLoan function*/
			return currentLoans.get(bookId);
		
		return null;
	}

	
	public double calculateOverDueFine(Loan Loan) {
		if (Loan.isOverDue()) {
			long daysOverDue = Calendar.getInstance().getDaysDifference(Loan.getDueDate());
			double fInE = daysOverDue * finePerDay;
			return fInE;
		}
		return 0.0;		
	}


	public void dischargeLoan(Loan currentLoan, boolean isDamaged) { /*DiScHaRgE_LoAn->dischargeLoan*/ /*cUrReNt_LoAn->currentLoan varibale*/ /*iS_dAmAgEd->isDamaged Varibale*/
		Member member = currentLoan.getMember();
		Book book  = currentLoan.getBook();
		
		double overDueFine = calculateOverDueFine(currentLoan);
		member.addFine(overDueFine);	
		
		member.dischargeLoan(currentLoan);
		book.ReTuRn(isDamaged);
		if (isDamaged) {
			member.addFine(damageFee);
			damagedBooks.put(book.getId(), book);
		}
		currentLoan.discharge(); /*DiScHaRgE->discharge function*/
		currentLoans.remove(book.getId());
	}


	public void checkCurrentLoans() {  /*cHeCk_CuRrEnT_LoAnS->checkCurrentLoan function*/
		for (Loan loan : currentLoans.values()) 
			loan.isCheckOverDue();
				
	}


	public void repairBook(Book currentBook) { /*RePaIr_BoOk->repairBooks*/  /*cUrReNt_BoOk->allCurrentBooks*/
		if (damagedBooks.containsKey(currentBook.getId())) {
			currentBook.repair();
			damagedBooks.remove(currentBook.getId());
		}
		else 
			throw new RuntimeException("Library: repairBook: book is not damaged");
		
		
	}
	
	
}
