package library.entities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Member implements Serializable {
    private String lastName;//LaSt_NaMe is changed to lastName--Arjun
    private String firstName;//FiRsT_NaMe is changed to firstName--Arjun
    private String emailAddress;//EmAiL_AdDrEsS is changed to emailAddress--Arjun
    private int phoneNumber;//PhOnE_NuMbEr is changed to phoneNumber--Arjun
    private int memberId;//MeMbEr_Id is changed to memberId--Arjun
    private double finesOwing;//FiNeS_OwInG is changed to finesOwing--Arjun
	
    private Map<Integer, Loan> currentLoans;//cUrReNt_lOaNs is changed to currentLoans

	
    public Member(String lastName, String firstName, String emailAddress, int phoneNumber, int memberId) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.memberId = memberId;
        this.currentLoans = new HashMap<>();
    }

	
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Member:  ").append(memberId).append("\n")
          .append("  Name:  ").append(lastName).append(", ").append(firstName).append("\n")
          .append("  Email: ").append(emailAddress).append("\n")
          .append("  Phone: ").append(phoneNumber)
          .append("\n")
          .append(String.format("  Fines Owed :  $%.2f", finesOwing))
          .append("\n");
		
        for (Loan loan : currentLoans.values()) {//local variable changed--Arjun
            sb.append(loan).append("\n");
        }		  
        return sb.toString();
    }

	
    public int getId() {//GeT_ID is changed to getId--Arjun
        return memberId;
    }

	
    public List<Loan> getLoans() {//GeT_LoAnS is changed to getLoans--Arjun
        return new ArrayList<Loan>(currentLoans.values());
    }

	
    public int getNumberOfCurrentLoans() {//gEt_nUmBeR_Of_CuRrEnT_LoAnS is changed to getNumberOfCurrentLoans--Arjun
        return currentLoans.size();
    }

	
    public double finesOwed() {//FiNeS_OwEd is changed to finesOwed--Arjun
        return finesOwing;
    }

	
    public void takeOutLoan(Loan loan) {
            /*Method name changed and local variable changed--Arjun*/
        if (!currentLoans.containsKey(loan.getId())) 
            currentLoans.put(loan.getId(), loan);
		
        else 
            throw new RuntimeException("Duplicate loan added to member");
				
    }

	
    public String getLastName() {//GeT_LaSt_NaMe is changed to getLastName--Arjun
        return lastName;
    }

	
    public String getFirstName() {//GeT_FiRsT_NaMe is changed to getFisrtName--Arjun
        return firstName;
    }


    public void addFine(double fine) {//AdD_FiNe is changed to addFine--Arjun
        finesOwing += fine;
    }
	
    public double payFine(double amount) {
            /* payFine is changed to payFine. 
            AmOuNt is changed to amount--Arjun*/
        if (amount < 0) 
            throw new RuntimeException("Member.payFine: amount must be positive");
		
        double change = 0;
        if (amount > finesOwing) { //FiNeS_OwInG is changed to finesOwing--Arjun
            change = amount - finesOwing;
            finesOwing = 0;
        }
        else 
            finesOwing -= amount;
		
        return change;
    }


    public void dischargeLoan(Loan loan) { //dIsChArGeLoAn is changed to dischargeLoan--Arjun
        if (currentLoans.containsKey(loan.getId())) 
            currentLoans.remove(loan.getId());	
        else 
            throw new RuntimeException("No such loan held by member");			
    }

}
