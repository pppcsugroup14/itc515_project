//Edited by arjun
package library.payfine;
import library.entities.Library;
import library.entities.Member;

public class PayFineControl {
	
	private PayFineUI ui;
	private enum ControlState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private ControlState state;
	private Library library;//LiBrArY-->library changed Arjun
	private Member member;//MeMbEr-->member changed --Arjun


	public PayFineControl() {

		this.library = library.getInstance();//GeTiNsTaNcE-->getInstance changed --Arjun
		state = ControlState.INITIALISED;

	}
	
	
	public void setUi(PayFineUI ui) {//SeT_uI-->setUi and uI-->ui changed --Arjun
		if (!state.equals(ControlState.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.ui = ui;
		ui.setState(PayFineUI.UiState.READY);//SeT_StAtE-->setState and UI_State-->UiState changed --Arjun
		state = ControlState.READY;		
	}


	public void cardSwiped(int memberId) {//CaRd_sWiPeD-->cardSwiped and MeMbEr_Id-->memberId chnaged --Arjun 
		if (!state.equals(ControlState.READY))
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);//gEt_MeMbEr-->getMember changed Arjun
		
		if (member == null) {
			ui.display("Invalid Member Id");
			return;
		}
		ui.display(member.toString());
		ui.setState(PayFineUI.UiState.PAYING);
		state = ControlState.PAYING;
	}
	
	
	public void cancel() { 
		ui.setState(PayFineUI.UiState.CANCELLED);
		state = ControlState.CANCELLED;
	}


	public double payFine(double amount) {
		if (!state.equals(ControlState.PAYING)) 
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double ChAnGe = member.payFine(amount);
		if (ChAnGe > 0) 
			ui.display(String.format("Change: $%.2f", ChAnGe));
		
		ui.display(member.toString());
		ui.setState(PayFineUI.UiState.COMPLETED);
		state = ControlState.COMPLETED;
		return ChAnGe;
	}
	


}
