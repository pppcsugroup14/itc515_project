//Changed By Gopal Bogati
package library.returnBook;
import library.entities.Book;
import library.returnBook.ReturnBookUI;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

	private ReturnBookUI Ui;
	private enum controlState { INITIALISED, READY, INSPECTING };//cOnTrOl_sTaTe->controlState//cOnTrOl_sTaTe->controlState//cOnTrOl_sTaTe->controlState//cOnTrOl_sTaTe->controlState
	private controlState state;
	private Library library;//lIbRaRy->library
	private Loan currentLoan;
	

	public ReturnBookControl() {
		this.library = Library.getInstance();
		state = controlState.INITIALISED;
	}
	
	
	public void setUi(ReturnBookUI uI) { //sEtUI->setUi
		if (!state.equals(controlState.INITIALISED)) 
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		
		this.Ui = uI;
		uI.setState(ReturnBookUI.uiState.READY);
		state = controlState.READY;		
	}


	public void bookScanned(int bookId) {
		if (!state.equals(controlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		
		Book currentBook = library.getBook(bookId);
		
		if (currentBook == null) {
			Ui.display("Invalid Book Id");
			return;
		}
		if (!currentBook.isOnLoan()) {
			Ui.display("Book has not been borrowed");
			return;
		}		
		currentLoan = library.getLoanByBookId(bookId);	
		double overDueFine = 0.0;
		if (currentLoan.isOverDue()) 
			overDueFine = library.calculateOverDueFine(currentLoan);//Over_Due_Fine->overDueFine
		
		Ui.display("Inspecting");
		Ui.display(currentBook.toString());
		Ui.display(currentLoan.toString());//CurrENT_loan->currentLoan
		
		if (currentLoan.isOverDue()) 
			Ui.display(String.format("\nOverdue fine : $%.2f", overDueFine));
		
		Ui.setState(ReturnBookUI.uiState.INSPECTING);
		state = controlState.INSPECTING;	//CoNtrolState->controlState	
	}


	public void scanningComplete() { //sCaNnInG_cOmPlEtE->scanningComplete
		if (!state.equals(controlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
			
		Ui.setState(ReturnBookUI.uiState.COMPLETED);		
	}


	public void dischargeLoan(boolean isDamaged) {//dIsChArGe_lOaN->dischargeLoan, isDamaged->isDamaged
		if (!state.equals(controlState.INSPECTING)) 
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		
		library.dischargeLoan(currentLoan, isDamaged);
		currentLoan = null;
		Ui.setState(ReturnBookUI.uiState.READY);
		state = controlState.READY;				
	}


}
