//Changed By Gopal Bogati
package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

    public static enum uiState { INITIALISED, READY, INSPECTING, COMPLETED };//UISTATE->uiState
        
        private ReturnBookControl control;//ContROL->control
	private Scanner input;//iNput->input
	private uiState state;//StAte->state

	
	public ReturnBookUI(ReturnBookControl control) {
		this.control = control;//CoNtrol->control
		input = new Scanner(System.in);
		state = uiState.INITIALISED;
		control.setUi(this);
	}


	public void run() {	//RuN->run	
		output("Return Book Use Case UI\n");//oUtPuT->output
		while (true) {
		    switch (state) { //StATe->state
		        case INITIALISED:
			    break;
				
			case READY:
			     String bookInputString = input("Scan Book (<enter> completes): ");//BoOk_InPuT_StRiNg->bookInputString
			        if (bookInputString.length() == 0) 
					control.scanningComplete();
				
				else {
					try {
						int bookId = Integer.valueOf(bookInputString).intValue();//Book_Id->bookId
						control.bookScanned(bookId);
				            }
					catch (NumberFormatException e) {
						output("Invalid bookId");
					    }					
				      }
				       break;				
				
				
			case INSPECTING:
			     String ans = input("Is book damaged? (Y/N): ");//iNpUt->input
				boolean isDamaged = false;//Is_DAmAgEd->isDamaged
				    if (ans.toUpperCase().equals("Y")) 	//AnS->ans				
					isDamaged = true;
				
				     control.dischargeLoan(isDamaged);


			
			case COMPLETED:
			      output("Return processing complete");
				 return;
			
			default:
			      output("Unhandled state");
			      throw new RuntimeException("ReturnBookUI : unhandled state :" + state);			
			}
		}
	}

	
	private String input(String prompt) {//PrOmPt->prompt
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void output(Object object) {//ObJeCt->object
		System.out.println(object);
	}
	
			
	public void display(Object object) {//DIsplay->display
		output(object);
	}
	
	public void setState(uiState state) {//SETsTATE->setState
		this.state = state;
	}

	
}
