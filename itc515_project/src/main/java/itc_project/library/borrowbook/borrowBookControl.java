
//Edited By Kripa
package itc_project.library.borrowbook;
import java.util.ArrayList;
import java.util.List;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl { /*bORROW_bOOK_cONTROL->BorrowBookControl*/
	
    private BorrowBookUI userId;
    private Library library;/*lIbRaRy->library*/
    private Member member;/*mEmBeR->member*/
    private enum CONTROL_STATE { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
    private CONTROL_STATE state;/*sTaTe->state*/
    private List<Book> pendingList;/*pEnDiNg_LiSt->pendingList*/
    private List<Loan> completedList;/*cOmPlEtEd_LiSt->completedList*/
    private Book book;/*bOoK->book*/
    public BorrowBookControl() {
	this.library = Library.getInstance();
	state = CONTROL_STATE.INITIALISED;
    }

    public void setUi(BorrowBookUI Ui) {/*SeT_Ui->setUi*/
	if (!state.equals(CONTROL_STATE.INITIALISED)) 
            throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
            this.userId = Ui;/*uI->userId*/
            Ui.setState(BorrowBookUI.uiState.READY);/*SeT_StAtE->setState*/  /*uI_STaTe->uiState*/
            state = CONTROL_STATE.READY;		
    }
		
    public void swiped(int memberId) { /*SwIpEd->swiped*/   /*mEmBeR_Id->memberId*/
        if (!state.equals(CONTROL_STATE.READY)) 
            throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
            member = library.getMember(memberId); /*gEt_MeMbEr->getMember*/
            if (member == null) {
		userId.display("Invalid memberId"); /*DiSpLaY->display*/
		return;
            }
            if (library.canMemberBorrow(member)) { /*cAn_MeMbEr_BoRrOw->canMemberBorrow*/
		pendingList = new ArrayList<>();
		userId.setState(BorrowBookUI.uiState.SCANNING);
		state = CONTROL_STATE.SCANNING; 
            }
	else {
            userId.display("Member cannot borrow at this time");
            userId.setState(BorrowBookUI.uiState.RESTRICTED); 
	}
    }
    public void scanned(int bookId) { /*ScAnNeD->scanned*/   /*bOoKiD->bookId*/
	book = null;
	if (!state.equals(CONTROL_STATE.SCANNING)) 
            throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
            book = library.getBook(bookId); /*gEt_BoOk->getBook*/
            if (book == null) {
		userId.display("Invalid bookId");
		return;
            }
            if (!book.isAvailable()) {
                userId.display("Book cannot be borrowed");
		return;
            }
            pendingList.add(book);
            for (Book B : pendingList) 
                userId.display(B.toString());
		if (library.getNumberOfLoansRemainingForMember(member) - pendingList.size() == 0) { /*gEt_NuMbEr_Of_LoAnS_ReMaInInG_FoR_MeMbEr->getNumberOfLoansRemainingForMember*/
                    userId.display("Loan limit reached");
                    complete(); /*CoMpLeTe->complete*/
		}
    }
	
    public void complete() {
	if (pendingList.size() == 0) 
            cancel(); /*CaNcEl->cancel*/
	else {
            userId.display("\nFinal Borrowing List");
            for (Book book : pendingList) 
		userId.display(book.toString()); /*bOoK->book*/
		completedList = new ArrayList<Loan>();
		userId.setState(BorrowBookUI.uiState.FINALISING);
		state = CONTROL_STATE.FINALISING;
	}
    }
    public void commitLoans() { /*CoMmIt_LoAnS->commitLoans*/
	if (!state.equals(CONTROL_STATE.FINALISING)) 
            throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
            for (Book B : pendingList) {
		Loan loan = library.issueLoan(B, member); /*lOaN->loan*/  /*iSsUe_LoAn->issueLoan*/
		completedList.add(loan);			
            }
            userId.display("Completed Loan Slip");
            for (Loan LOAN : completedList) 
                userId.display(LOAN.toString());
		userId.setState(BorrowBookUI.uiState.COMPLETED);
		state = CONTROL_STATE.COMPLETED;
    }
    public void cancel() {
	userId.setState(BorrowBookUI.uiState.CANCELLED);
	state = CONTROL_STATE.CANCELLED;
    }
}