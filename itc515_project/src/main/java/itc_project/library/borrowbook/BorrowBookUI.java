//Edited By Kripa
package itc_project.library.borrowbook;
import java.util.Scanner;
import  itc_project.library.borrowbook.BorrowBookUI;
import  itc_project.library.borrowbook.BorrowBookControl;

public class BorrowBookUI {
	
    public static enum uiState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
        private BorrowBookControl control;/*CoNtRoL->control*/
        private Scanner input;/*InPuT->input*/
        private uiState state;/*StaTe->state*/
        
        public BorrowBookUI(BorrowBookControl control) {
            this.control = control;
            input = new Scanner(System.in);
            state = uiState.INITIALISED;
            control.setUi(this);
	}
	private String input(String prompt) { /*iNpUT->input*/ /*PrOmPt->prompt*/
            System.out.print(prompt);
            return input.nextLine();
	}	
	private void output(Object object) {/*OuTpUt->output*/  /*ObJeCt->object*/
            System.out.println(object);
	}
	public void setState(uiState state) { /*StAtE->state*/
            this.state = state;
	}
	public void run() {/*RuN->run*/
            output("Borrow Book Use Case UI\n");
            while (true) {
		switch (state) {			
                    case CANCELLED:
			output("Borrowing Cancelled");
			return;
                    case READY:
			String memStr = input("Swipe member card (press <enter> to cancel): ");/*MEM_STR->memStr*/
			if (memStr.length() == 0) {
                            control.cancel();
                            break;
			}
			try {
                            int memberId = Integer.valueOf(memStr).intValue();/*MeMbEr_Id->memberId*/
                            control.swiped(memberId);
                        }
			catch (NumberFormatException e) {
                            output("Invalid Member Id");
                        }
			break;
			case RESTRICTED:
                            input("Press <any key> to cancel");
                            control.cancel();
                            break;
			case SCANNING:
                            String bookStringInput = input("Scan Book (<enter> completes): "); /*BoOk_StRiNg_InPuT->bookStringInput*/
                            if (bookStringInput.length() == 0) {
				control.complete();
				break;
                            }
                            try {
				int bId = Integer.valueOf(bookStringInput).intValue();/*BiD->bId*/
                            	control.scanned(bId);
                            } catch (NumberFormatException e) {
				output("Invalid Book Id");
                            } 
                            break;
			case FINALISING:
                            String ans = input("Commit loans? (Y/N): ");/*AnS->ans*/
                            if (ans.toUpperCase().equals("N")) {
				control.cancel();
                            } else {
				control.commitLoans();
				input("Press <any key> to complete ");
                            }
                            break;
			case COMPLETED:
                            output("Borrowing Completed");
                            return;
			default:
                            output("Unhandled state");
                            throw new RuntimeException("BorrowBookUI : unhandled state :" + state);			
		}
            }		
	}
	public void display(Object object) {
            output(object);		
	}
}
